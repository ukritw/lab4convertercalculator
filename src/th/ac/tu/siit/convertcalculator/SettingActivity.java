package th.ac.tu.siit.convertcalculator;

import java.util.Locale;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SettingActivity extends Activity implements OnClickListener {
	
	float exchangeRate;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting);
		
		Button b1 = (Button)findViewById(R.id.btnFinish);
		b1.setOnClickListener(this);
		
		Intent i = this.getIntent();
		exchangeRate = i.getFloatExtra("exchangeRate", 32.0f);
		
		if (i.getStringExtra("activity").equals("From_ExchangeActivity")) {
			TextView title = (TextView)findViewById(R.id.textView1);
			title.setText("Exchange Rate");
		} else if (i.getStringExtra("activity").equals("From_InterestActivity")) {
			TextView title = (TextView)findViewById(R.id.textView1);
			title.setText("Interest Rate");
		} 
		
		EditText etRate = (EditText)findViewById(R.id.etRate);
		etRate.setText(String.format(Locale.getDefault(), "%.2f", exchangeRate));
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.setting, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		EditText etRate = (EditText)findViewById(R.id.etRate);
		try {
			float r = Float.parseFloat(etRate.getText().toString());
			// Create an intent to wrap the return value
			Intent data = new Intent();
			// Set the intent as the result when this activity ends
			data.putExtra("exchangeRate", r);
			this.setResult(RESULT_OK, data);
			// End this activity
			this.finish();
		} catch(NumberFormatException e) {
			Toast t = Toast.makeText(getApplicationContext(), 
					"Invalid interest rate", Toast.LENGTH_SHORT);
			t.show();
			etRate.setText("");
			etRate.requestFocus();
		} catch(NullPointerException e) {
			Toast t = Toast.makeText(getApplicationContext(), 
					"Invalid interest rate", Toast.LENGTH_SHORT);
			t.show();
			etRate.setText("");
			etRate.requestFocus();
		}
	}

}
