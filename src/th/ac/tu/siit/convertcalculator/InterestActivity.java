package th.ac.tu.siit.convertcalculator;

import java.util.Locale;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class InterestActivity extends Activity implements OnClickListener {
	
	float interestRate = 10.0f;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_interest);
		
		Button b1 = (Button)findViewById(R.id.calculate);
		b1.setOnClickListener(this);
		
		Button b2 = (Button)findViewById(R.id.settings);
		b2.setOnClickListener(this);
		
		TextView interests = (TextView)findViewById(R.id.interestrate);
		interests.setText(String.valueOf(interestRate) + "%");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.interest, menu);
		return true;
	}
	
	@Override
	public void onClick(View v) {
		int id = v.getId();
		
		if (id == R.id.calculate) {
			EditText usdET = (EditText)findViewById(R.id.USD);
			EditText yearsET = (EditText)findViewById(R.id.years);
			TextView afterTV = (TextView)findViewById(R.id.after);
			String res = "";
			try {
				float usd = Float.parseFloat(usdET.getText().toString());
				float years = Float.parseFloat(yearsET.getText().toString());
				float result = (float) (usd*Math.pow((1.0f+(interestRate/100)), years));
				res = String.format(Locale.getDefault(), "%.2f", result);
			} catch(NumberFormatException e) {
				res = "Invalid input";
			} catch(NullPointerException e) {
				res = "Invalid input";
			}
			afterTV.setText(res);
		}
		else if (id == R.id.settings) {
			Intent i = new Intent(this, SettingActivity.class);
			i.putExtra("exchangeRate", interestRate);
			i.putExtra("activity","From_InterestActivity");  
			startActivityForResult(i, 9999);
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 9999 && resultCode == RESULT_OK) {
			interestRate = data.getFloatExtra("exchangeRate", 10.0f);
			TextView interests = (TextView)findViewById(R.id.interestrate);
			interests.setText(String.valueOf(interestRate) + "%");
		}
	}
}
